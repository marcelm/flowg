from collections import Counter

class Squeezer:
	def __init__(self, characters='ACGT'):
		"""
		characters -- characters to include in the histogram
		"""
		self.runlengths = Counter()
		self.characters = set(characters)

	def squeeze(self, s):
		"""
		Return a run-length compressed version of string s.
		"""
		squeezed = ''
		prev = None
		count = 0
		for c in s:
			if c != prev:
				squeezed += c
				if prev in self.characters:
					self.runlengths[count] += 1
				count = 1
				prev = c
			else:
				count += 1
		self.runlengths[count] += 1
		return squeezed

#		total += len(seq)
#		total_acgt += len(seq) - len(seq.translate(None, 'ACGT'))
#		total_squeezed += len(squeezed)
