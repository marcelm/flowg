#!/bin/bash
set -xe
#python setup.py build_ext -i
nosetests "$@" -vsd --with-coverage --cover-erase --cover-package=. --cover-inclusive
