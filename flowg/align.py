#!/usr/bin/env python3
"""
Alignment of flowgrams against DNA sequences.
"""
from array import array
import logging

log = logging.getLogger(__name__)


def red(s):
	"""add ANSI escapes to string s to make it white on red"""
	# background red + foreground white + s + reset to normal
	return "\x1b[0;41m" + "\x1b[1;37m" + s + "\x1b[0m"


class FlowgramAlignment:
	"""
	An alignment of a flowgram to a string.
	"""
	def __init__(self, flows, seq, lengths, total_score, scoring):
		"""
		intensities -- list that contains flows as (character, float) tuples and None values.
		seq -- list of the strings of the reference that the flows are aligned to.
		lengths -- actual length of each run (determined by score function)
		scoring -- scoring object that was used. A scoring.score_edit function must exist.
		"""
		assert len(flows) == len(seq) == len(lengths)
		self.flows = flows
		self.seq = seq
		self.lengths = lengths
		self.scoring = scoring
		self.score = total_score

	def print(self):
		for fl, s, my_len in zip(self.flows, self.seq, self.lengths):
			marker = False
			if fl is None:
				print('----- {}  DELETION {: .2f}             '.format(s.decode(), self.scoring.s_del), end='')
				marker = True
			else:
				ch, intensity = fl
				if int(intensity + 0.5) != len(s) or \
						len(set(s)) > 1 or \
						(len(s) > 0 and ch != s[0]):
					marker = True
				# Re-compute scores.
				sc, length = self.scoring.score(ch, intensity, s)
				s_ed = self.scoring.score_edit(ch, length, s)
				s_il = self.scoring.score_intensity_length(intensity, length)
				print("{}{:.2f} {:8s} {:2d} {: .2f} = {: .2f}{:+.2f}".format(
					chr(ch), intensity, s.decode(), length, sc, s_ed, s_il), end='')
			if marker:
				print('  <---- editing/measurement error')
			else:
				print()


	def basecall(self):
		"""Return the basecalled sequence, using the information in the flowgram"""
		result = bytearray()
		for flow, length in zip(self.flows, self.lengths):
			if flow is None:
				# a deletion
				continue
			ch, _ = flow
			result.extend([ch] * length)
		return result


if False:
	sequence = None
	def prefix_substitution_scores(sequence):
		"""
		Compute substitution scores for each
		alphabet character.
		subst[c][j] is the score of substituting all characters
		within sequence[0:j] with the character c.
		"""
		n = len(sequence)
		subst = {}
		for c in b"ACGT":
			a = array('h', (n + 1) * [0])
			p = 0
			for j in range(n):
				p = p + (scoring.s_mat if sequence[j] == c else scoring.s_mis)
				a[j + 1] = p
			subst[c] = a
		return subst

	subst = prefix_substitution_scores(sequence)
	log.debug("subst:")
	if log.isEnabledFor(logging.DEBUG):
		for c in subst:
			log.debug("substitution scores:")
			log.debug("%r %r", chr(c), subst[c])



def flowalign(flowgram, sequence, scoring):
	"""
	Global alignment of a flowgram to a reference sequence.

	flowgram -- a Flowgram object
	sequence -- reference sequence (bytes)
	"""
	flow_chars, flow_values = flowgram.flowchars, flowgram.intensities
	log.debug("running flowalign()")
	log.debug("  flowgram: %s", flowgram)
	log.debug("  sequence: %r", sequence.decode())
	assert len(flow_chars) == len(flow_values)

	#           seq (n, j)
	#      --------------->
	#     |
	#  fg | (m, i)
	#     |
	#     V
	m = len(flowgram)
	n = len(sequence)
	origsequence = sequence
	sequence = sequence.upper()

	rows = [ array('f', (n + 1) * [0]) for _ in range(m + 1) ]

	# encoding for directions in the backtrace table:
	# backtrace[i][j] == k for k <= 0 means that (i, j) points to (i-1, j+k)
	# backtrace[i][j] == 1 means that (i, j) points to (i, j-1)
	LEFT = 1
	backtrace = [ array('h', (n + 1) * [LEFT]) for _ in range(m + 1) ]
	lengths = [ array('h', (n + 1) * [-1]) for _ in range(m + 1) ]

	for j in range(1, n + 1):
		rows[0][j] = scoring.s_del * j
	for i in range(1, m + 1):
		backtrace[i][0] = 0
		sc, l = scoring.score(flow_chars[i - 1], flow_values[i - 1], b'')
		rows[i][0] = rows[i - 1][0] + sc
		lengths[i][0] = l

	assert rows[0][0] == 0
	LOOK_BACK = 12  # TODO

	# skip first row, which already contains the correct scores
	for i in range(1, m + 1):
		for j in range(1, n + 1):
			# create a list of all scores
			# regular deletion of a single nucleotide
			scores = [ (rows[i][j - 1] + scoring.s_del, LEFT, -1) ]

			for k in range(max(0, j - LOOK_BACK), j + 1):  # +1 to also get the empty suffix
				sc, l = scoring.score(flow_chars[i - 1], flow_values[i - 1], sequence[k:j])
				scores.append((rows[i - 1][k] + sc, k - j, l))
			best = max(scores)
			rows[i][j] = best[0]
			backtrace[i][j] = best[1]
			lengths[i][j] = best[2]

	if __debug__:
		# check whether all fields in the backtrace table contain a sensible value
		for j in range(1, n + 1):
			assert backtrace[0][j] == LEFT
		for i in range(m + 1):
			for j in range(n + 1):
				b = backtrace[i][j]
				assert (b == LEFT) or (b <= 0 and b + j >= 0), "({}, {}) = {}".format(i, j, b)

	if log.isEnabledFor(logging.DEBUG):
		# TODO the path is not marked correctly!
		print("Dynamic programming table: (NOTE: marked cells are only the maxima in each row, not the correct path)")
		print(" " * 6 + ' '.join('{:5d}'.format(x) for x in range(n + 1)))
		print(" " * 16 + '     '.join(origsequence.decode()))
		for i in range(m + 1):
			if i >= 1:
				print("{} {:5.2f} ".format(chr(flow_chars[i - 1]), flow_values[i - 1]), end='')
			else:
				print(" " * 8, end='')
			best = max(rows[i])
			for j in range(n + 1):
				text = "{:5.1f} ".format(rows[i][j])
				if rows[i][j] == best:
					text = red(text)
				print(text, end='')
			print()

	# backtrace

	# max_j = rows[-1].index(max(rows[-1]))  # enable for semiglobal alignment

	aflows = []
	aseq = []
	alengths = []
	while i > 0 or j > 0:
		# encoding for directions in the backtrace table:
		# backtrace[i][j] == k for k <= 0 means that (i, j) points to (i-1, j+k)
		# backtrace[i][j] == 1 means that (i, j) points to (i, j-1)
# 		score = rows[i][j]
		direction = backtrace[i][j]
		if direction == LEFT:
			j -= 1
			aflows.append(None)
			aseq.append(sequence[j:j + 1])
			alengths.append(None)
		elif direction <= 0:
			x = lengths[i][j]
			assert x >= -1
			i -= 1
			j += direction
			aflows.append((flow_chars[i], flow_values[i]))
			aseq.append(sequence[j:j - direction])
			alengths.append(None if x == -1 else x)
		else:
			assert False, "direction invalid"
	assert i == j == 0
	aflows.reverse()
	aseq.reverse()
	alengths.reverse()
	return FlowgramAlignment(aflows, aseq, alengths, rows[-1][-1], scoring)
