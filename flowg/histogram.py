"""
working with histogram files
"""
import numpy as np


def read_histogram(f):
	"""
	Read a histogram from a file and return it as numpy matrix.

	histogram[l,f] is the count for length l, intensity f/100.
	That is, histogram[2,140] tells us how often intensity 1.40 was
	aligned to a run of two nucleotides.
	"""
	a = np.zeros( (50, 16384), np.int32 )
	for line in f:
		if line.startswith('#'):
			continue
		fields = line.split()
		length = int(fields[0])
		intensity = int(100*float(fields[1])+0.5)
		count = int(fields[2])
		assert a[length, intensity] == 0
		a[length, intensity] = count
	return a
