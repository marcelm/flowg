"""
Scoring functions.

Two versions of scoring are implemented: with and without preprocessing.
"""
# import numpy -- this is loaded only when  PreprocessedScoring is used
from functools import lru_cache

# Empirical scoring parameters derived for GCB paper:
# a list of 5-tuples.
# _GCB_PARAMS[i] describes parameters for length "i":
# (center, lefttop, leftslope, righttop, rightslope)
nan = None
_GCB_PARAMS = [
	(0.660, nan, nan, 9.993, -17.903),
	(1.058, 11.819, 26.378, 7.919, -14.780),
	(2.341, 12.581, 21.812, 5.620, -9.875),
	(3.566, 12.702, 18.746, 6.972, -9.428),
	(4.691, 11.219, 13.639, 11.642, -12.514),
	(5.802, 8.824, 8.931, 8.279, -5.984),
	(6.889, 10.255, 7.924, 9.208, -5.713),
	(7.557, 9.025, 4.739, 8.010, -2.880),
]

SCORE_MATCH = 1.2
SCORE_MISMATCH = -3.1
SCORE_DELETION = -8
SCORE_INSERTION = SCORE_DELETION


def get_gcb_scoring(preprocessing=True):
	"""
	Return a Scoring object, using pre-defined parameters.
	"""
	if preprocessing:
		return PreprocessedScoring(SCORE_INSERTION, SCORE_DELETION, SCORE_MISMATCH, SCORE_MATCH)
	else:
		return Scoring(SCORE_INSERTION, SCORE_DELETION, SCORE_MISMATCH, SCORE_MATCH)


class Scoring:
	"""
	Scoring without preprocessing: Appropriate when only a few scores need to
	be calculated. This is written to be understandable, not fast.

	The class also uses some caching (via functools.lru_cache), but
	the version below that includes preprocessing is still faster.
	"""
	def __init__(self, s_ins, s_del, s_mis, s_mat):
		"""
		s_ins -- insertion score
		s_del -- deletion score
		s_mis -- mismatch score
		s_mat -- match score
		"""
		assert s_ins < s_mis < 0 < s_mat
		assert s_del < s_mis
		self.s_ins = s_ins
		self.s_del = s_del
		self.s_mis = s_mis
		self.s_mat = s_mat

	def score_intensity_length(self, intensity, length, params=_GCB_PARAMS):
		"""
		Return score for aligning a flow of intensity "f"
		against a piece of sequence of length "length".

		This is \sigma(f, l) in the GCB paper.
		"""
		N = len(params)
		p = params[length if length < N else N - 1]  # parameters for given length
		delta = intensity - length
		top = p[0]
		if -0.5 < delta < 0.5:
			return top  # intensity == length
		s = p[1] + delta * p[2]  if delta <= -0.5  else p[3] + delta * p[4]
		return s if s < top else top

	def score_edit(self, ch, l, t):
		"""
		Return edit score of aligning t to ch repeated l times.

		ch -- char
		l -- length of run
		t -- substring on reference. Indels are relative to it.

		This is s_edit(b, l, t) in the GCB paper.
		"""
		# TODO with Python 3.3, t.count(ch) would work
		c = t.count(bytes([ch]))
		len_t = len(t)
		non_c = len_t - c
		if l >= len_t:
			return c * self.s_mat + non_c * self.s_mis + (l - len_t) * self.s_ins
		else:
			return min(l, c) * self.s_mat + (len_t - l) * self.s_del + max(l - c, 0) * self.s_mis

	# Limiting the cache size is necessary to avoid MemoryErrors
	@lru_cache(maxsize=1000000)
	def score(self, b, intensity, t):
		"""
		Total score: editing plus measurement error.

		b -- flow character
		intensity -- flow intensity
		t -- sequence that is paired with this flow

		Return a (score, length) pair, where length is the length of the run
		that was found to give the optimal score. Thus, the correct sequence
		of this part of the read is predicted to be b repeated length times.

		This is v(b, f, t) in the GCB paper.
		"""
		return max(
				(self.score_edit(b, length, t) + self.score_intensity_length(intensity, length), length)
				for length in range(0, 15)
			)


class PreprocessedScoring(Scoring):
	"""
	Scoring with preprocessing. The initialization takes a few seconds,
	but then individual lookups take only constant time. Use this
	when many scores need to be calculated.
	"""
	LEN = 15  # scores for strings shorter than length LEN are cached
	I = 1500  # scores up to 15.00 are cached

	def __init__(self, s_ins, s_del, s_mis, s_mat):
		"""
		s_ins -- insertion score
		s_del -- deletion score
		s_mis -- mismatch score
		s_mat -- match score
		"""
		Scoring.__init__(self, s_ins, s_del, s_mis, s_mat)
		self._fill_cache()

	def _fill_cache(self, LEN=LEN, I=I):
		# load numpy only if this class is actually used
		import numpy as np

		cache = np.empty(LEN * LEN * I)
		cache.shape = (LEN, LEN, I)
		cache_length = np.empty(LEN * LEN * I, dtype=int)
		cache_length.shape = (LEN, LEN, I)
		for lt in range(LEN):
			for c in range(lt + 1):
				for i in range(I):
					v = self._score(c, lt, i / 100)
					cache[c, lt, i] = v[0]
					cache_length[c, lt, i] = v[1]
		self.cache = cache
		self.cache_length = cache_length

	def _score(self, c, len_t, intensity):
		"""
		Compute total score.

		This starts with a zero-length string and iteratively adds characters to it.
		For each length, the score is determined. This is faster than calling a
		function such as Scoring.score_edit again and again for each length.

		In this Python version, the savings result, for example, because len(t) and
		t.count(c) are called only once each (and passed in as parameters here).
		"""
		assert 0 <= c <= len_t < 15
		editscore = len_t * self.s_del  # start out with 'delete everything'
		length = 0
		scores = [ self.score_intensity_length(intensity, length) + editscore ]

		# incrementally undo deletions
		while length < c:
			# we lose one deletion and gain one match
			editscore = editscore - self.s_del + self.s_mat
			length += 1
			scores.append(self.score_intensity_length(intensity, length) + editscore)

		# also undo deletions of non-c characters
		while length < len_t:
			# we lose one deletion and gain one mismatch
			editscore = editscore - self.s_del + self.s_mis
			length += 1
			scores.append(self.score_intensity_length(intensity, length) + editscore)

		# further edits are insertions
		while length < 15:  # TODO hardcoded
			editscore += self.s_ins
			length += 1
			scores.append(self.score_intensity_length(intensity, length) + editscore)

		assert len(scores) == 16
		m = max(scores)
		return m, scores.index(m)

	def score(self, b, intensity, t, LEN=LEN, I=I):
		"""
		Return the total score. If the score is in the cache, only a lookup
		is required and the score is returned immediately.
		"""
		c = t.count(b)
		len_t = len(t)
		i = int(intensity * 100 + 0.5)
		if len_t < LEN and i < I:
			return self.cache[c, len_t, i], self.cache_length[c, len_t, i]
		else:
			return self._score(c, len_t, intensity)


def edit_score_by_aligning(s, t, s_ins, s_del, s_mis, s_mat):
	"""
	Return the score of an optimal alignment between the strings s and t.
	Indels are relative to s.

	This uses dynamic programming and requires O(len(s) * len(t)) time.
	This function is not used in the actual scoring, only for testing.
	"""
	m = len(s)  # s is indexed by i
	n = len(t)  # t is indexed by j

	# A column in the dynamic programming table
	# Using an array('h', ...) here does not result in a speedup.
	scores = list(s_del * i for i in range(m + 1))

	# Fill the table
	prev = 0
	for j in range(1, n + 1):
		prev = scores[0]
		scores[0] += s_ins
		for i in range(1, m + 1):
			c = max(
				prev + (s_mat if s[i - 1] == t[j - 1] else s_mis),
				scores[i] + s_ins,
				scores[i - 1] + s_del)
			prev = scores[i]
			scores[i] = c
	return scores[-1]


def test_score_intensity_length():
	scoring = get_gcb_scoring()
	length = 4
	intensity = 2.8
	while intensity < 5.2:
		s = scoring.score_intensity_length(intensity, length)
		print("{:.2f}: {:.3f}".format(intensity, s))
		intensity += 0.01


if __name__ == "__main__":
	test_score_intensity_length()
