"""
We have two representations of a flowgram: One is the one as chosen in
SFF files (SFFRead object), the other is a list of
(character, intensity) tuples (Flowgram object).
"""
from collections import namedtuple
import logging
log = logging.getLogger()

class Flow(namedtuple('Flow', ['char', 'intensity'])):
	def __str__(self):
		return '{}{:.2f}'.format(chr(self.char), self.intensity)


class Flowgram:
	"""
	A flowgram is mathematically defined to be a list of (char, intensity) pairs,
	but this implementation stores two lists, one with chars and one
	with intensities. Indexed access and iteration are supported, however, and result in
	(char, intensity) pairs.
	"""
	def __init__(self, flowchars, intensities):
		if len(flowchars) != len(intensities):
			raise ValueError("lengths of flowchars and intensities must be equal")
		self.flowchars = flowchars
		self.intensities = intensities

	def __iter__(self):
		return (Flow(*flow) for flow in zip(self.flowchars, self.intensities))

	def __len__(self):
		return len(self.flowchars)

	def __str__(self):
		return ' '.join(str(flow) for flow in self)


def parse_flowgram(s):
	"""
	Parse string s given as "T3.7 A0.2 C1.1" into a flowgram.
	Return a Flowgram object.
	"""
	g = list(zip(*[ (ord(f[0]), float(f[1:])) for f in s.split() ]))
	if g:
		return Flowgram(bytes(g[0]), g[1])
	else:
		return Flowgram(b'', ())


def parse_example(s):
	"""
	Parse a string like this:

	CCGA   C2.25 G1.18 T0.02 A0.97  #  a comment

	into a tuple (reference, flowgram, comment).

	The comment is set to an empty string if it is not found.
	Reference is a bytes object.

	This is mainly useful for testing.
	"""
	fields = s.split('#', 1)
	if len(fields) > 1:
		comment = fields[1].strip()
	else:
		comment = ''
	reference, rest = fields[0].split(None, 1)
	reference = reference.encode('ascii')
	flowgram = parse_flowgram(rest)
	return (reference, flowgram, comment)
