#!/usr/bin/env python3
"""
Merge, that is, add multiple histograms
"""
import sys
from argparse import ArgumentParser
from flowg.histogram import read_histogram


def main():
	parser = ArgumentParser(description=__doc__)
	parser.add_argument("histograms", nargs='+',
		help="path to histogram files with length, intensity, frequency data")
	args = parser.parse_args()

	histogram = None
	for path in args.histograms:
		with open(path) as infile:
			h = read_histogram(infile)  # numpy matrix
			if histogram is None:
				histogram = h
			else:
				histogram += h
	for length in range(histogram.shape[0]):
		for intensity in range(len(histogram[length])):
			freq = histogram[length,intensity]
			if freq == 0:
				continue
			print(length, intensity / 100, freq)


if __name__ == '__main__':
	main()
