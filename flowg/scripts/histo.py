#!/usr/bin/env python3
"""
length, intensity, frequency
"""
import sys
from itertools import islice
from collections import Counter
import logging

from pysam import Samfile, IndexedReads

from sqt.io.sff import SFFFile
from sqt.io.fasta import IndexedFasta
from sqt.dna import reverse_complement
from sqt import HelpfulArgumentParser

from sqt.cigar import print_alignment, unclipped_region, decoded_ops
from sqt.cigar import as_string as cigar_as_string
from flowg.flowgram import Flowgram, ClippedRead



def align_stat(sffread, bamread, fasta, samfile):
	"""
	Use an aligned read from a BAM file and the original flowgram of that read to
	estimate the correspondence between actual hompolymer length and measured
	flow intensity.

	Return a dictionary (a Counter object) that has tuples
	(character, intensity, homopolymer_length) as keys and maps them to absolute frequencies.
	The character is one of A, C, G and T. The intensity is a value between
	0 and 655.35 (=(2**16-1)/100.0). The homopolymer_length is a nonnegative integer.
	"""
	bambases = bamread.seq
	bamcigar = bamread.cigar
	if bamread.is_reverse:
		bambases = reverse_complement(bambases)
		bamcigar = bamcigar[::-1]

	# apply soft clipping
	bamcigar, unclipped_start, unclipped_stop = unclipped_region(bamcigar)
	bambases = bambases[unclipped_start:unclipped_stop]
	clipped = ClippedRead(sffread, unclipped_start, unclipped_stop)
	assert clipped.bases == bambases

	fastaseq = fasta.get(samfile.getrname(bamread.tid))[bamread.pos:bamread.aend]
	fastaseq = fastaseq.upper()
	if bamread.is_reverse:
		fastaseq = reverse_complement(fastaseq)

	if DEBUG:
		print("Read name:      ", sffread.name)
		print("bases:          ", bambases)
		print("BAM clipped:    ", bamread.query)
		print("FASTA:          ", fastaseq)
		print("flowgram:       ", Flowgram(clipped))
		print("CIGAR:          ", cigar_as_string(bamcigar))
		print("base_to_flow:   ", clipped.base_to_flow)
		print_alignment(bambases, fastaseq, bamcigar)

	read = bambases
	ref = fastaseq
	ops = list(decoded_ops(bamcigar))

#	fl = iter(clipped.base_to_flow)
#	intensity = iter(clipped.intensities)
#	c = iter(clipped.flowchars)

	b2fl = clipped.base_to_flow

	counter = Counter()
	read_index = 0
	ops_index = 0
	ref_index = 0

	# fl: index into flowgram

	"""
	flowchar    C    G    T    A    C    G   T
	intensity 1.1  0.1  1.8  0.1  1.0  0.1 2.8
	read        C       TT-         C      TTT
	cigar       M       MMD         M      MMI
	ref         C       TTT         C      TT-

	"""
	# iterate over the flows of this flowgram
	for fl, (char, intensity) in enumerate(zip(clipped.flowchars, clipped.intensities)):
		# Within each flow, we need to iterate over its alignment
		# to the reference (one column in the above diagram).
		# If any of the characters encountered in the read or the reference
		# are not the same as the character in the current flowgram (...?)

		# seq will contain the reference sequence corresponding to this
		# subalignment.
		seq = ''

		# if there's anything strange with the alignment, this will be set to True
		discard = False

		while read_index < len(read) and b2fl[read_index] == fl:

			# consume one base from the read
#			assert read[read_index] == char

			# Find the next 'M' or 'I' cigar operation.
			# For deletions, check whether the reference base is correct.

			# Consume deletions
			while ops_index < len(ops) and ops[ops_index] == 'D':
				if ref[ref_index] != char:
					discard = True
				ops_index += 1
				seq += chr(ref[ref_index])
				ref_index += 1
			if ops_index < len(ops) and ops[ops_index] == 'M':
				# consume one reference base
				if ref[ref_index] != char:
					discard = True
				ops_index += 1
				seq += chr(ref[ref_index])
				ref_index += 1
			elif ops_index < len(ops) and ops[ops_index] == 'I':
				ops_index += 1
			else:
				assert ops_index == len(ops), "unknown CIGAR operator"
			
			# consume deletions again
			while ops_index < len(ops) and ops[ops_index] == 'D' and ref[ref_index] == char:
				ops_index += 1
				seq += chr(ref[ref_index])
				ref_index += 1

			read_index += 1

		runlength = len(seq)
		if DEBUG:
			marker = '' if int(intensity + 0.5) == runlength else '<------------'
			marker += ' IGNORED' if discard else ''
			print("{}{:.2f} {} {} {}".format(chr(char), intensity, runlength, seq, marker))
		assert discard or (len(seq) == 0 or len(set(seq)) == 1 and seq[0] == chr(char))
		if not discard:
			counter[ (char, intensity, runlength)] += 1

	return counter


def print_histogram(counts, file=sys.stdout):
	"""
	Print histogram in text format to a file.
	"""
	l = ( (length, intensity, frequency) for (intensity, length), frequency in counts.items() )
	for item in sorted(l):
		print(*item, file=file)


def get_argument_parser():
	p = HelpfulArgumentParser()
	p.add_argument("-d", "--debug", action='store_true')
	p.add_argument("--limit", metavar="N", type=int, default=None, help="Stop after N reads")
	p.add_argument('bam', metavar="BAM", help="mapped reads")
	p.add_argument('fasta', metavar="FASTA", help="reference")
	p.add_argument('sff', metavar="SFF", help="same reads as those in the BAM")
	return p


def main():
	args = get_argument_parser().parse_args()
	global log
	global DEBUG
	if args.debug:
		level = logging.DEBUG
		DEBUG = True
	else:
		level = logging.INFO
		DEBUG = False
	logging.basicConfig(level=level)
	log = logging.getLogger()
	
	fasta = IndexedFasta(args.fasta)
	sff = SFFFile(args.sff)
	bam = Samfile(args.bam)

	log.debug("no of reads in SFF: %r", sff.number_of_reads)
	# the index allows us to retrieve a read by its name
	index = IndexedReads(bam)
	index.build()

	counter = Counter()
	# iterate over SFF file, get aligned read from index
	i = 0
	try:
		for sffread in islice(sff, 0, args.limit):
			log.debug("read name: %r", sffread.name)
			try:
				bamreads = list(index.find(sffread.name))
			except KeyError:
				log.warn("read %r not found", sffread.name)
				continue
			if len(bamreads) > 0:
				bamread = bamreads[0]
				if bamread.is_unmapped:
					continue
				counter.update(align_stat(sffread, bamread, fasta, bam))
			i += 1
			if i % 1000 == 0:
				print("read", i, file=sys.stderr)
	except KeyboardInterrupt:
		# if we get interrupted, stop processing and just print
		# out the result we have so far
		pass
	# go over counter and aggregate over characters
	count = Counter()
	for char, intensity, length in counter:
		count[(intensity, length)] += counter[(char, intensity, length)]

	# write result to stdout
	print_histogram(count)


if __name__ == '__main__':
	main()
