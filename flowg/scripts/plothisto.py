#!/usr/bin/env python3
"""
Plot intensity-vs.-homopolymer-length histograms.

The plot types are:

freq -- frequency only (logarithmic y axis)
logodds -- log-odds score  log p(f,l) / (p(f) * p(l))
normal -- p(f | l) -- that is, the curve for each homopolymer length is normalized to an area of one

For the 'normal' type, also the normal distributions given by
Balzer et al. (2010) in Table 2 are shown.

Balzer et al.: Characteristics of 454 pyrosequencing data–enabling
realistic simulation with flowsim.
"""
import sys
from argparse import ArgumentParser
import matplotlib as mpl
mpl.use('pdf')  # enable matplotpib over an ssh connection without X
import matplotlib.pyplot as plt
import numpy as np
from itertools import cycle
from flowg.histogram import read_histogram


def empirical_scores(H, mincount=50, delta=None):
	"""
	H -- histogram
	
	(previous value: delta = 1.0)
	"""
	P = H / np.sum(H)  # joint probability, matrix sums to 1.0.
	intensity_prior = np.sum(P, axis=0)  # i.e. sum over axis 0
	length_prior = np.sum(P, axis=1)
	S = np.zeros_like(H, dtype=np.double)
	for length in range(H.shape[0]):
		p = np.copy(P[length,:])  # row for a given genomic length
		h = H[length,:]
		p[h < mincount] = np.nan
		if delta is not None:
			p[:max(0, int(100*(length-delta)))] = np.nan
			p[int(100*(length+delta))+1:] = np.nan
		S[length,:] = np.log(p / (length_prior[length] * intensity_prior))
		#print(S[length,:10])
	return S


def plot_empirical_scores(S, scale=None, styles=None, pdf=None,
	                      XMAX=11, YMAX=12):
	if styles is None:
		styles = ('r-', 'b--', 'g-', 'k:')
		#styles = ('k-', 'k--', 'k:', 'k.')
		#styles = ('r-', 'b--', 'g:', 'r--', 'b:', 'g-')
	fig = plt.figure(figsize=(146/25.4, 100/25.4))
	ax = fig.add_subplot(111)
	# for each length, plot score function
	for length, style in zip(range(S.shape[0]), cycle(styles)):
		data = S[length,:]
		if scale is not None:
			# scale by given factor and round to closest integer
			data = np.around(data * scale, 0)
		ax.plot(np.arange(0, len(data)/100, 0.01), data, style, linewidth=2) #color=cm.jet(i*100))
	ax.grid(True)
	ax.set_xlim(-0.1, XMAX)
	ax.set_xticks(np.arange(0, XMAX, 1))
	ax.format_xdata = lambda s: "%.2f" % (s/100)
	ax.set_xlabel('Flow intensity')
	if scale is not None:
		ax.set_ylim(-YMAX*scale, +YMAX*scale)
		ax.set_ylabel('Score (Scaling factor: %d)' % scale)
	else:
		ax.set_ylim(-YMAX, YMAX)
		ax.set_ylabel('Score')
	ax.legend(["Length {}".format(i) for i in range(XMAX+1)], loc=4, prop={'size':9})
	plt.tight_layout()
	if pdf:
		plt.savefig(pdf)
	else:
		plt.show()
	plt.close()


def plot_score_over_length(S, pdf=None, XMAX=11, YMAX=12):
	styles = ('r--', 'b--', 'g-', 'k--', 'y', 'r-', 'b-', 'g--', 'k-', 'y--', 'r-.', 'b-.')
	fig = plt.figure(figsize=(146/25.4, 68/25.4))
	ax = fig.add_subplot(111)
	intensities = np.arange(0, 801, 100)
	for intensity, style in zip(intensities, cycle(styles)):  # intensities 0.0, 0.2, 0.4, ..., 4.00
		data = S[:,intensity]
		ax.plot(np.arange(len(data)), data, style, linewidth=1) #color=cm.jet(i*100))
		#ax.plot(np.arange(len(data)), data, style[0] + '.', linewidth=2) #color=cm.jet(i*100))
	ax.grid(True)
	ax.set_xlim(-0.1, XMAX)
	ax.set_xticks(np.arange(0, XMAX, 1))
	#ax.format_xdata = lambda s: "%.2f" % (s/100)
	ax.set_xlabel('Genomic length')
	ax.set_ylim(-YMAX, YMAX)
	ax.set_ylabel('Score')
	ax.legend(["Intensity {:.2f}".format(i/100) for i in intensities], loc=4, prop={'size':9})
	plt.tight_layout()
	if pdf:
		plt.savefig(pdf)
	else:
		plt.show()
	plt.close()


def get_argument_parser():
	parser = ArgumentParser(description=__doc__)
	parser.add_argument("-t", "--type", choices=('freq', 'logodds', 'normal', 'sol'), default='freq',
		help="type of plot. freq: frequencies; logodds: log-odds scores; normal: each homopolymer length normalized to area of one with the empirical normal distribution from Balzer et al.")
	parser.add_argument("--scale", type=int,
		help="scale score by given factor and round to closest integer (default: no rounding, no scaling)")
	parser.add_argument("histogram",
		help="path to file with length, intensity, frequency data")
	parser.add_argument("pdf", nargs='?',
		help="PDF output file. If not given, show plot interactively.")
	return parser


def main():
	parser = get_argument_parser()
	args = parser.parse_args()
	mode = args.type

	with open(args.histogram) as infile:
		histogram = read_histogram(infile)
		# histogram is a #dnalengths x #intensities numpy array (int32)

	if mode == 'logodds':
		scores = empirical_scores(histogram, mincount=50)
		plot_empirical_scores(scores, scale=args.scale, pdf=args.pdf)
		sys.exit(0)
	if mode == 'sol':
		scores = empirical_scores(histogram, mincount=50)
		plot_score_over_length(scores, YMAX=10, pdf=args.pdf)
		sys.exit(0)

	fig = plt.figure(figsize=(146/25.4, 100/25.4),)
	ax = fig.add_subplot(111)

	# for each length, plot graph of desired type (mode)
	styles = ('r-', 'b--', 'g-', 'k:')
	for length in range(histogram.shape[0]):
		s = np.sum(histogram[length])
		if s < 2000:
			# skip lengths with too few data points
			continue
		if mode == 'freq':
			data = histogram[length]
		else:
			# normalize such that the area equals 1
			data = histogram[length] / (0.01 * np.sum(histogram[length]))
			#   or: / np.max(histogram[length])
		print(data[:10])
		ax.plot(np.arange(0, len(data)/100, 0.01), data, styles[length%len(styles)], linewidth=2) #color=cm.jet(i*100))

	if mode == 'normal':
		# mu, sigma of empirical intensity distribution
		# copied from Balzer et al. (2010), Table 2: Characteristics of 454
		# pyrosequencing data–enabling realistic simulation with flowsim.
		normal_parameters = [
			(0.1230, 0.0737),  # n=0
			(1.0193, 0.1227),  # n=1
			(2.0006, 0.1585),  # n=2
			(2.9934, 0.2188),  # n=3
			(3.9962, 0.3168),  # n=4
			(4.9550, 0.3863),  # n=5
		]
		for n in range(6, 10):
			normal_parameters.append( (n, 0.03494 + 0.06856 * n) )

		i = 0
		styles = [ 'k', 'k--' ]
		for mu, sigma in normal_parameters:
			x = np.arange(mu - 3, mu + 3, 0.01)
			y = 1 / (sigma * np.sqrt(2 * np.pi)) * np.exp( -(x - mu)**2 / (2 * sigma**2) )
			ax.plot(x, y, styles[i%len(styles)])
			i += 1

	ax.grid(True)
	XMAX = 11
	ax.set_xlim(-0.1, XMAX)
	ax.set_xticks(np.arange(0, XMAX, 1))
	ax.format_xdata = lambda s: "%.2f" % (s/100)
	ax.set_xlabel('Flow intensity')

	if mode == 'logodds':
		if args.scale is not None:
			ax.set_ylim(-15 * args.scale, +15 * args.scale)
			ax.set_ylabel('Score (Scaling factor: %d)' % args.scale)
		else:
			#ax.set_xlim(0, XMAX)
			ax.set_ylim(-15, +12)
			ax.set_ylabel('Score')
	elif mode == 'freq':
		ax.set_yscale('log')
		ax.set_ylim(5, None)
		ax.set_ylabel('Frequency')
	else:
		ax.set_ylim(0, None)
		ax.set_ylabel('p(intensity | length)')

	plt.tight_layout()
	if args.pdf:
		plt.savefig(args.pdf)
	else:
		plt.show()


if __name__ == '__main__':
	main()
