#!/usr/bin/env python3
"""
Create per-nucleotide histograms of flowgram intensities.
"""
from sqt.io.sff import SFFFile
from collections import Counter
from sqt import HelpfulArgumentParser
import sys


def process(sffpath, limit=None):
	"""
	Process a single SFF file
	"""
	bases = b'ACGT'
	counters = {}
	for c in bases:
		counters[c] = Counter()

	for i, read in enumerate(SFFFile(sffpath)):
		assert len(read.intensities) == len(read.flowchars)
		for intensity, char in zip(read.intensities, read.flowchars):
			counters[char][intensity] += 1
		if i % 2000 == 0:
			print(i, "sequences processed", file=sys.stderr)
		if i == limit:
			break

	for i in range(32768):
		counts = [ counters[b][i/100] for b in bases ]
		if sum(counts) > 0:
			print("{:.2f}".format(i/100), counts[0], counts[1], counts[2], counts[3])


def main():
	parser = HelpfulArgumentParser(description=__doc__)
	parser.add_argument('--limit', '-n', metavar='N', default=None, type=int,
		help="Limit processing to first N reads in the file")
	parser.add_argument('sff', metavar='SFF', help="Name of SFF file")
	args = parser.parse_args()
	process(args.sff, args.limit)


if __name__ == '__main__':
	main()
