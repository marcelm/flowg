#!/usr/bin/env python3
"""
Flowgram alignment.

You can directly give a reference and a flowgram as command-line parameters,
or read them from an input file.

Examples:

bin/flowg-align TCCCG "T1.2 A0.1 C3.6 G1.0"
bin/flowg-align -i examples.txt
"""
import sys
import logging
from argparse import ArgumentParser, RawDescriptionHelpFormatter, FileType

from flowg.flowgram import parse_example, parse_flowgram
from flowg.align import flowalign
from flowg.scoring import get_gcb_scoring

__author__ = "Marcel Martin"


class HelpfulArgumentParser(ArgumentParser):
	"""An ArgumentParser that prints full help on errors."""
	def error(self, message):
		self.print_help(sys.stderr)
		args = {'prog': self.prog, 'message': message}
		self.exit(2, '%(prog)s: error: %(message)s\n' % args)


def process(reference, flowgram, scoring):
	alignment = flowalign(flowgram, reference, scoring)
	print("Flowgram alignment (score {:.3f}):".format(alignment.score))
	alignment.print()

	# base-calling by rounding
	s = ''.join(int(intensity + 0.5) * chr(ch) for ch, intensity in flowgram)

	print()
	print('Reference:         ', reference.decode())
	print('Read (by rounding):', s)
	print('Re-called:         ', alignment.basecall().decode())
	print()
	print()

def get_argument_parser():
	p = HelpfulArgumentParser(description=__doc__, formatter_class=RawDescriptionHelpFormatter)
	p.add_argument("-d", "--debug", action='store_true')
	p.add_argument("-p", "--preprocessing", action='store_true',
		help="Enable score pre-processing. Adds a few seconds to startup, but is then faster."
			" Default: %(default)s")
	p.add_argument("-i", "--input", type=FileType('r'),
		help="Read in a file with multiple reference/flowgram pairs (one per line).")
	p.add_argument("reference", nargs='?',
		help="The reference string, such TCGGG")
	p.add_argument("flowgram", nargs='?',
		help='A flowgram, such as "T1.2 A0.1 C0.5 G3.6" (you may need to add the quote characters)')
	return p


def main():
	parser = get_argument_parser()
	args = parser.parse_args()
	logging.basicConfig(level=logging.DEBUG if args.debug else logging.INFO)

	if args.input is None and args.reference is None:
		parser.error("Need an input file or a reference and flowgram.")
	if args.reference is not None and args.flowgram is None:
		parser.error("Both reference and flowgram must be given.")

	if args.preprocessing:
		try:
			import numpy
			del numpy
		except ImportError:
			parser.error("You need to have numpy installed for preprocessed scoring.")

	scoring = get_gcb_scoring(preprocessing=args.preprocessing)

	if args.reference is not None:
		flowgram = parse_flowgram(args.flowgram)
		reference = args.reference.encode()
		process(reference, flowgram, scoring)
	if args.input:
		for line in args.input:
			line = line.strip()
			if len(line) == 0 or line.startswith('#'):
				continue
			reference, flowgram, comment = parse_example(line)
			print("========>", comment)
			process(reference, flowgram, scoring)


if __name__ == '__main__':
	main()
