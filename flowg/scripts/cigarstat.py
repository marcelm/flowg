#!/usr/bin/env python3
"""
Read a BAM file and print a histogram of the CIGAR operations that occur in all alignments.
"""
import sys
from collections import Counter
from pysam import Samfile
from sqt import cigar

from sqt import HelpfulArgumentParser

__author__ = "Marcel Martin"


def main():
	parser = HelpfulArgumentParser(description=__doc__)
	parser.add_argument("sam", metavar="SAM/BAM", help="SAM or BAM file")
	args = parser.parse_args()

	filename = args.sam
	mode = 'r' if filename.endswith('.sam') else 'rb'
	infile = Samfile(filename, mode)

	counter = Counter()
	for record in infile:
		if record.cigar is not None:
			for op, l in record.cigar:
				counter[op] += l
	ops = 'MIDNSHP'
	for op in sorted(counter):
		print("{:2} {:9}".format(ops[op], counter[op]))
	return 0

if __name__ == '__main__':
	sys.exit(main())
