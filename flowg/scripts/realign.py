#!/usr/bin/env python3
"""
Re-align reads and output a re-basecalled sequence in FASTA format for each
input read.
"""
import sys
from itertools import islice
import logging
from sqt.io.sff import SFFFile
from sqt.io.fasta import IndexedFasta, FastaReader
from sqt.dna import reverse_complement
from sqt import HelpfulArgumentParser
from pysam import Samfile, IndexedReads

from sqt.cigar import print_alignment, unclipped_region
from sqt.cigar import as_string as cigar_as_string
from sqt.io.sff import ClippedRead
from flowg.align import flowalign
from flowg.scoring import get_gcb_scoring

__author__ = "Marcel Martin"


def prefix_edit_distance(s, t):
	"""
	Return the edit distance between the strings s and t.
	The edit distance is the sum of the numbers of insertions, deletions,
	and mismatches that is minimally necessary to transform one string
	into the other. Trailing characters of s are ignored.
	"""
	m = len(s)  # index: i
	n = len(t)  # index: j

	# dynamic programming "table" (just a single column)
	# note that using an array('h', ...) here is not faster
	costs = list(range(m + 1))

	# calculate alignment (using unit costs)
	prev = 0
	for j in range(1, n + 1):
		prev = costs[0]
		costs[0] += 1
		for i in range(1, m + 1):
			c = min(
				prev + int(s[i - 1] != t[j - 1]),
				costs[i] + 1,
				costs[i - 1] + 1)
			prev = costs[i]
			costs[i] = c
	return min(costs)


def realign_read(sffread, bamread, fasta, samfile, scoring):
	"""
	Use an aligned read from a BAM file and the original flowgram of that read to
	estimate the correspondence between actual hompolymer length and measured
	flow intensity.
	"""
	bambases = bamread.seq
	bamcigar = bamread.cigar
	if bamread.is_reverse:
		bambases = reverse_complement(bambases)
		bamcigar = bamcigar[::-1]

	# apply soft clipping
	bamcigar, unclipped_start, unclipped_stop = unclipped_region(bamcigar)
	# if unclipped_start > 0 or unclipped_stop is not None:
		# print("CLIPPING:", unclipped_start, unclipped_stop)
	bambases = bambases[unclipped_start:unclipped_stop]
	clipped = ClippedRead(sffread, unclipped_start, unclipped_stop)
	assert clipped.bases == bambases

	fastaseq = fasta.get(samfile.getrname(bamread.tid))[bamread.pos:bamread.aend]
	fastaseq = fastaseq.upper()
	if bamread.is_reverse:
		fastaseq = reverse_complement(fastaseq)

	if log.isEnabledFor(logging.DEBUG):
		print("Read name:      ", sffread.name)
		print("bases:          ", bambases)
		print("BAM clipped:    ", bamread.query)
		print("FASTA:          ", fastaseq)
		print("flowgram:       ", str(clipped))
		print("CIGAR:          ", cigar_as_string(bamcigar))
		print_alignment(bambases, fastaseq.upper(), bamcigar)

	alignment = flowalign(clipped, fastaseq, scoring)

	if log.isEnabledFor(logging.DEBUG):
		alignment.print()
	return alignment, unclipped_start, unclipped_stop, bambases


def get_argument_parser():
	p = HelpfulArgumentParser(description=__doc__)
	p.add_argument("-d", "--debug", action='store_true')
	p.add_argument("--truth", help="FASTA file with truth")
	p.add_argument("--limit", metavar="N", type=int, default=None, help="Stop after N reads")
	p.add_argument('bam', metavar="BAM")
	p.add_argument('fasta', metavar="FASTA", help='FASTA reference')
	p.add_argument('sff', metavar="SFF")
	return p


def main():
	args = get_argument_parser().parse_args()
	global log
	if args.debug:
		level = logging.DEBUG
	else:
		level = logging.INFO
	logging.basicConfig(level=level)
	log = logging.getLogger()

	fasta = IndexedFasta(args.fasta)
	sff = SFFFile(args.sff)
	bam = Samfile(args.bam)

	if args.truth:
		truth = dict((record.name.split()[0], record.sequence) for record in FastaReader(args.truth, binary=True))
	else:
		truth = None

	log.debug("no of reads in SFF: %r", sff.number_of_reads)
	# the index allows us to retrieve a read by its name
	index = IndexedReads(bam)
	index.build()

	scoring = get_gcb_scoring()
	# iterate over SFF file, get aligned read from index
	i = 0
	try:
		for sffread in islice(sff, 0, args.limit):
			log.debug("read name: %r", sffread.name)
			try:
				bamreads = list(index.find(sffread.name))
			except KeyError:
				log.warn("read %r not found", sffread.name)
				continue
			if len(bamreads) > 0:
				if len(bamreads) > 1:
					log.warn("read %r found more than once", sffread.name)
				bamread = bamreads[0]
				if bamread.is_unmapped:
					continue
				alignment, unclipped_start, unclipped_stop, bambases = \
					realign_read(sffread, bamread, fasta, bam, scoring)
				realigned_sequence = alignment.basecall()
				if truth:
					true_sequence = truth[sffread.name][unclipped_start:]
					true_sequence = true_sequence[:len(realigned_sequence) + 20]
					unclean_dist = prefix_edit_distance(true_sequence, bambases)
					cleaned_dist = prefix_edit_distance(true_sequence, realigned_sequence)
					print('>{}-{}-{} edit distance: before {} after {}\n{}'.format(
						sffread.name,
						unclipped_start,
						unclipped_stop,
						unclean_dist,
						cleaned_dist,
						realigned_sequence.decode('ascii'),
						))
					# print(true_sequence.decode('ascii'))
				else:
					print('>{}-{}:{}\n{}'.format(
						sffread.name,
						unclipped_start,
						unclipped_stop,
						realigned_sequence.decode('ascii'),
						))

			i += 1
			if i % 1000 == 0:
				print("read", i, file=sys.stderr)
	except KeyboardInterrupt:
		# stop processing and just print out the result we have so far
		pass

	bam.close()
	fasta.close()


if __name__ == '__main__':
	main()
