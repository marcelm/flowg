#!/usr/bin/env python3
"""
"""
import sys
from argparse import ArgumentParser
from collections import Counter
import matplotlib as mpl
#mpl.use('pdf')  # enable matplotpib over an ssh connection without X
import matplotlib.pyplot as plt
import numpy as np

def get_argument_parser():
	parser = ArgumentParser(description=__doc__)
	parser.add_argument("pdf", nargs='?',
		help="PDF output file. If not given, show plot interactively.")
	return parser

def main():
	parser = get_argument_parser()
	args = parser.parse_args()

	N = 20
	counts_before = [0] * N
	counts_after = [0] * N
	for line in sys.stdin:
		fields = line.split()
		before, after = int(fields[0]), int(fields[1])
		counts_before[before] += 1
		counts_after[after] += 1

	print('before:', counts_before)
	print('after:', counts_after)
	
	fig = plt.figure(figsize=(6, 3))
	ax = fig.add_subplot(111)

	ind = np.arange(N)
	r_before = ax.bar(ind, counts_before, 0.4, color='b')
	r_after = ax.bar(ind+0.4, counts_after, 0.4, color='r')
	ax.set_xticks(ind + 0.4)
	ax.set_xticklabels(list(range(N)))
	ax.legend( (r_before[0], r_after[0]), ('Naive basecalling', 'Flowgram alignment') )
	ax.set_xlabel('Number of differences')
	ax.set_ylabel('Frequency')
	ax.set_xlim((0, 6))
	plt.tight_layout()
	plt.savefig(args.pdf)
	return
		
	# for each length, plot graph of desired type (mode)
	styles = [ 'r-', 'b--', 'g:', 'r--', 'b:', 'g-']
	for length in range(histogram.shape[0]):
		s = np.sum(histogram[length])
		if s < 2000:
			# skip lengths with too few data points
			continue
		if mode == 'logodds':
			data = np.log(p[length] / length_prior[length] / intensity_prior)

			if args.scale is not None:
				# scale by given factor and round to closest integer
				data = np.around(data * args.scale, 0)
		elif mode == 'freq':
			data = histogram[length]
		else:
			# normalize such that the area equals 1
			data = histogram[length] / (0.01 * np.sum(histogram[length]))
			#   or: / np.max(histogram[length])
		print(data[:10])
		ax.plot(np.arange(0, len(data)/100, 0.01), data, styles[length%len(styles)], linewidth=2) #color=cm.jet(i*100))

	if mode == 'normal':
		# mu, sigma of empirical intensity distribution
		# copied from Balzer et al. (2010), Table 2: Characteristics of 454
		# pyrosequencing data–enabling realistic simulation with flowsim.
		normal_parameters = [
			(0.1230, 0.0737),  # n=0
			(1.0193, 0.1227),  # n=1
			(2.0006, 0.1585),  # n=2
			(2.9934, 0.2188),  # n=3
			(3.9962, 0.3168),  # n=4
			(4.9550, 0.3863),  # n=5
		]
		for n in range(6, 10):
			normal_parameters.append( (n, 0.03494 + 0.06856 * n) )

		i = 0
		styles = [ 'k', 'k--' ]
		for mu, sigma in normal_parameters:
			x = np.arange(mu - 3, mu + 3, 0.01)
			y = 1 / (sigma * np.sqrt(2 * np.pi)) * np.exp( -(x - mu)**2 / (2 * sigma**2) )
			ax.plot(x, y, styles[i%len(styles)])
			i += 1

	ax.grid(True)
	XMAX = 12
	ax.set_xlim(-0.1, XMAX)
	ax.set_xticks(np.arange(0, XMAX, 1))
	ax.format_xdata = lambda s: "%.2f" % (s/100)
	ax.set_xlabel('Flow intensity')

	if mode == 'logodds':
		if args.scale is not None:
			ax.set_ylim(-15 * args.scale, +15 * args.scale)
			ax.set_ylabel('Score (Scaling factor: %d)' % args.scale)
		else:
			ax.set_ylim(-15, 15)
			ax.set_ylabel('Score')
	elif mode == 'freq':
		ax.set_yscale('log')
		ax.set_ylim(1, None)
		ax.set_ylabel('Frequency')
	else:
		ax.set_ylim(0, None)
		ax.set_ylabel('p(intensity | length)')

	plt.tight_layout()
	if args.pdf:
		plt.savefig(args.pdf)
	else:
		plt.show()


if __name__ == '__main__':
	main()
