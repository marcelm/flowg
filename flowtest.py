#!/usr/bin/env python3
from sqt.io.sff import SFFFile
import sys
from collections import Counter
from itertools import islice

bases = 'ACGT'
counters = {}
for c in bases:
	counters[c] = Counter()

f = SFFFile(sys.argv[1])
print(f)
print(dir(f))
print(f.key_sequence)
print(f.reads())
print(f.number_of_reads)

for read in islice(f, 0, 4):
	print()
	print("Read name:", read.name)
	print("bases:")
	print(read.bases)
	print(read.intensities)
	print(read.clip)
	print(read.flowchars)
	print(read.bases)
	print(read.bases[read.insert_start:read.insert_stop])
	print(read.flow_index_per_base)
	j = 0
	next = read.flow_index_per_base[j]
	for i, (c, f) in enumerate(zip(read.flowchars, read.intensities)):
		if i == next:
			s = j
			j += 1
			while j < len(read.flow_index_per_base) and read.flow_index_per_base[j] == 0:
				j += 1
			if j == len(read.flow_index_per_base):
				next = None
			else:
				next += read.flow_index_per_base[j]
		else:
			s = ''
		print("{:3} {:1} {:5.2f} {:3} {}".format(i, chr(c), f, s, int(f*5)*'*'))
		
		

		
		