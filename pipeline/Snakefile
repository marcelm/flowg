# kate: syntax Python;
"""
Simulate E. coli reads for evaluation of flowgram alignment.

Binaries needed:
* sqt-fastamutate
* kitsim
* clonesim
* sff2fasta
* bwa
* samtools
* flowg-histo
* flowg-realign
* fasta_formatter
"""
shell.prefix("set -o pipefail;")

rule:
	input: "reads-1k.cleaned.edhisto"

rule download_ecoli:
	output: "EcoliK12.fasta"
	shell: "echo 'Please download the E. coli K12 FASTA sequence from http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nuccore&id=NC_000913&rettype=fasta&retmode=text' && exit 1"

rule:
	output: "ecoli.fa"
	input: "EcoliK12.fasta"
	shell: "fasta_formatter -w 80 < {input} | sed '1s/ .*//' > {output}"

rule:
	output: "reads-{number}k.orig.fa"
	input: "ecoli.fa"
	shell:
		r"""clonesim -c {wildcards.number}000 {input} | awk '/^>/ {{i+=1;printf("%s:%d %s\n", $1, i, $2)}} !/^>/' | sed 's| |_|g' > {output}"""

rule:
	output: "{base}.mutated.fa"
	input: "{base}.orig.fa"
	shell: "sqt-fastamutate --rate 0.03 --indel-rate 0.0005 {input} > {output}"

rule:
	output: "reads-{base}.sff"
	input: "reads-{base}.fa"
	shell: "kitsim < {input} | flowsim -G GS20 -o {output}"

rule:
	output: "{base}.basecalled.fa"
	input: "{base}.sff"
	shell: "sff2fasta {input} > {output}"

rule index:
	input: "{base}.fa"
	output: "{base}.fa.bwt"
	shell: "bwa index {input}"

rule bwasw:
	threads: 8
	output: bam="{base}.ubam"
	input: fa="{base}.basecalled.fa", ref="ecoli.fa", bwt="ecoli.fa.bwt"
	shell:
		"bwa bwasw -t {threads} {input.ref} {input.fa} | samtools view -bS - > {output.bam}"

rule sort:
	output: "{base}.bam"
	input: "{base}.ubam"
	shell: "samtools sort {input} {wildcards.base}"

rule faidx:
	output: "{base}.fa.fai"
	input: "{base}.fa"
	shell: "samtools faidx {input}"

rule histo:
	output: "{base}.histo"
	input: ref="ecoli.fa", fai="ecoli.fa.fai", bam="{base}.bam", sff="{base}.sff"
	shell: "flowg-histo {input.bam} {input.ref} {input.sff} > {output}"

rule clean:
	output: "{base}.cleaned.fa"
	input: truth="{base}.mutated.fa", bam="{base}.mutated.bam", ref="ecoli.fa", fai="ecoli.fa.fai", sff="{base}.mutated.sff"
	shell: "flowg-realign --truth {input.truth} {input.bam} {input.ref} {input.sff} > {output}"

rule cleanhisto:
	output: "{base}.cleaned.edhisto"
	input: "{base}.cleaned.fa"
	shell: """grep '^>' {input} | cut -d" " -f5,7 > {output}"""

