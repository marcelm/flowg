#!/usr/bin/env python3
"""
"""
__author__ = "Marcel Martin"

import sys
from sqt import HelpfulArgumentParser
from sqt.io.fasta import FastaReader
from squeezer import Squeezer


def main():
	parser = HelpfulArgumentParser(description=__doc__)
	parser.add_argument("--bisulfite", "-b", action='store_true', default=False,
		help="bisulfite mode (simulate C->T replacement)")
	parser.add_argument("fasta", metavar="FASTA", help="input FASTA file")
	args = parser.parse_args()

	if args.fasta == '-':
		infile = sys.stdin
	else:
		infile = args.fasta
	total = 0
	total_acgt = 0
	total_squeezed = 0
	squeezer = Squeezer()
	for record in FastaReader(infile):
		seq = record.sequence.upper()
		if options.bisulfite:
			seq = seq.replace('C', 'T')
		squeezed = squeezer.squeeze(seq)
		total += len(seq)
		total_acgt += len(seq) - len(seq.translate(None, 'ACGT'))
		total_squeezed += len(squeezed)
		seqio.writefasta(sys.stdout, [(record.name, squeezed)], 100)

	sys.stdout = sys.stderr
	print("bisulfite mode:", options.bisulfite)
	print("total:", total)
	print("total ACGT:", total_acgt)
	print("squeezed:", total_squeezed, total_squeezed/total_acgt*100, "%")
	print("run frequencies")
	total_5 = 0
	runlengths = squeezer.runlengths
	for k in sorted(runlengths):
		print("{:>4} {:>9} {:>6.4}%".format(k, runlengths[k], runlengths[k]*k / total_acgt * 100))
		if k >= 5:
			total_5 += runlengths[k] * k
	print("runs of at least length 5 in", total_5 / total_acgt * 100, "% of nucleotides")
	sys.stdout = sys.__stdout__


if __name__ == '__main__':
	sys.exit(main())
