Flowgram-string aligner
=======================

This is an implementation of flowgram-string alignment as proposed
in our paper submitted to GCB 2013: "Aligning Flowgrams to DNA Sequences".


License
=======

(This is the MIT license.)

Copyright (c) 2013
Marcel Martin <marcel.martin@tu-dortmund.de>
Sven Rahmann <sven.rahmann@uni-due.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.


Requirements
============

You need to have Python 3.2 or later. In order to use preprocessed scores,
numpy is required.


Running
=======

We recommend to run the script `bin/flowg-align` and to read the help
text. An example:

    python3 bin/flowg-align TCCCG "T1.2 A0.1 C3.6 G1.0"

This aligns the flowgram T1.2 A0.1 C3.6 G1.0 to the reference "TCCCG".
The output is:

```
Flowgram alignment (score 10.091):
T1.20 T      1   2.26 =  1.20+1.06
A0.10        0   0.66 =  0.00+0.66
C3.60 CCC    3   4.92 =  3.60+1.32  <---- editing/measurement error
G1.00 G      1   2.26 =  1.20+1.06

Reference:          TCCCG
Read (by rounding): TCCCCG
Re-called:          TCCCG
```

The alignment score 10.091 is shown in the first line. Then the flowgram
alignment is printed with one flow/string pair per row. The first row
pairs the flow T1.20 with the base T. The second row pairs flow A0.10
with the empty string. And the third row pairs the flow C3.60 with the
string CCC.

The column with 1, 0, 3, 1 gives the estimated correct length of the
run. The "3" in the above example means that probably three Cs in the
sample gave rise to intensity C3.6. Since this is an overcall, this
row is marked on the right.

The remaining columns give the score for that row.
2.26 = 1.20 + 1.06 means that the total score is 2.26, score due to
editing events is 1.20, and the score for the measurement is 1.06.

Finally, the reference, the naively base-called read (through rounding),
and also the corrected read (which uses the information in the length
column).


Input file
==========

It is also possible to read the reference and flowgram from a text
file. A text file `examples.txt` is provided in this directory. To
align all reference/flowgram pairs in the file, run this:

    python3 bin/flowg-align -i examples.txt
