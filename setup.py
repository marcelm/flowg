from setuptools import setup
import sys
from glob import glob
import os

__version__ = '0.1'

if sys.version < '3.2':
	sys.stdout.write("At least Python 3.2 is required (try running with 'python3 setup.py').\n")
	sys.exit(1)

setup(
	name = 'flowg',
	version = __version__,
	author = 'Marcel Martin',
	author_email = 'marcel.martin@tu-dortmund.de',
	url = '',
	description = '',
	#license = 'MIT',
	packages = ['flowg', 'flowg.scripts'],
	scripts = [ s for s in glob(os.path.join("bin", "flowg-*")) if not s.endswith('~') ],
	install_requires = [
		"numpy",
		"matplotlib",
		#"pysam",
		#"sqt",
		#"SQLalchemy >= 0.8.0b2",
	],
	tests_require = ['nose'],
	classifiers = [
		"Development Status :: 4 - Beta",
		#"Development Status :: 5 - Production/Stable",
		"Environment :: Console",
		"Intended Audience :: Science/Research",
		"License :: OSI Approved :: MIT License",
		"Natural Language :: English",
		"Programming Language :: Python :: 3",
		"Topic :: Scientific/Engineering :: Bio-Informatics"
	]
)
