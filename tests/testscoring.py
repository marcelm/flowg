from flowg.scoring import edit_score_by_aligning, Scoring

s_ins = -5
s_del = -4
s_mis = -2
s_mat = +1


def test_scoring():
	scoring = Scoring(s_ins, s_del, s_mis, s_mat)
	# empty reference (length = 0), read has two chars: two insertions
	sedit = scoring.score_edit
	assert sedit('X', 0, 'AA') == 2 * s_del
	assert sedit('X', 2, '') == 2 * s_ins
	for ref in (
		'', 'A', 'T', 'AA', 'AT', 'TA', 'TT',
		'AAA', 'AAT', 'ATA', 'ATT', 'TAA', 'TAT', 'TTA', 'TTT',
		'AAAA', 'ATAA', 'ATAT', 'TAAT', 'TTTA', 'TTAT', 'ATTT'):
		for l in range(10):
			# indels are relative to
			bl = l * 'A'
			s_edit_true = edit_score_by_aligning(ref, bl, s_ins, s_del, s_mis, s_mat)
			s_edit_found = scoring.score_edit('A', l, ref)
			assert s_edit_true == s_edit_found, \
				"->{}<- ->{}<-  true: {} found: {}".format(ref, bl, s_edit_true, s_edit_found)
