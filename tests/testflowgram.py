from flowg.flowgram import parse_flowgram, parse_example

A = ord('A')
C = ord('C')
G = ord('G')
T = ord('T')


def test_parse_flowgram():
	assert parse_flowgram("T1.0 A2.7  C5.5") == [ (T, 1.0), (A, 2.7), (C, 5.5) ]


def test_parse_example():
	reference, flowgram, comment = parse_example("CCGA   C2.25 G1.18  T1.02  #  a comment")
	assert reference == b'CCGA'
	assert flowgram == [ (C, 2.25), (G, 1.18), (T, 1.02) ]
	assert comment == 'a comment'
