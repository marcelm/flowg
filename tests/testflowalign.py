#!/usr/bin/env python3

from flowg.flowgram import parse_flowgram as parse
from flowg.align import flowalign
from flowg.scoring import get_gcb_scoring

scoring = get_gcb_scoring(preprocessing=False)

def align(flowgram, reference):
	return flowalign(flowgram, reference, scoring)


def test_vacic():
	'example given by Vacic et al. (2008)'
	flowgram = parse('C0.92 G0.34 T0.49 A0.32 C0.98')
	alignment = align(flowgram, b'CTC')
	print("bc:", alignment.basecall())
	assert alignment.basecall() == b'CTC'

