from squeezer import Squeezer

def test_squeezer():
	sq = Squeezer()
	sq.squeeze('AAAAACCCNGGGCTCAGG')
	assert sq.runlengths == { 1: 4, 2: 1, 3: 2, 5: 1 }
	